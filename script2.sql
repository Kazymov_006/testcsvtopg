-- Step 1: Create the target table if it doesn't exist
CREATE TABLE IF NOT EXISTS engines_dict (
                                            id INTEGER PRIMARY KEY,
                                            text TEXT
);
BEGIN;
TRUNCATE TABLE engines_dict;
\copy engines_dict FROM '/data/csv_storage/engine_types.csv' DELIMITER ',' CSV HEADER;
COMMIT;