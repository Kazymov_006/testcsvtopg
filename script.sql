-- Step 1: Create the target table if it doesn't exist
CREATE TABLE IF NOT EXISTS engines_dict (
    id INTEGER PRIMARY KEY,
    text TEXT
);

BEGIN;
-- begin transaction
CREATE TABLE IF NOT EXISTS tmp (
                                               id INTEGER PRIMARY KEY,
                                               text TEXT
);
-- Step 2: Copy data from the CSV file to a temporary table
\copy tmp FROM '/data/csv_storage/engine_types.csv' DELIMITER ',' CSV HEADER;

-- Step 3: Insert data from the temporary table into the target table, handling duplicates
INSERT INTO engines_dict (id, text)
SELECT * FROM tmp
    ON CONFLICT (id) DO UPDATE
        SET text = EXCLUDED.text;

DROP TABLE IF EXISTS tmp;
-- end transaction
COMMIT;
