FROM alpine:latest

RUN apk update && \
    apk add postgresql-client

WORKDIR /app/testCsvToPg

#COPY script.sql script.sql
COPY script2.sql script.sql

CMD psql postgresql://$PG_USER:$PG_PASSWORD@$PG_HOST:5432/$PG_DBNAME -f /app/testCsvToPg/script.sql
